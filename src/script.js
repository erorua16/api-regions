
// fetch ('https://geo.api.gouv.fr/regions')
// .then(res => res.json())
// .then (data => console.log(data))
// .catch(error => console.log('ERROR'))

// api url
const api_url ="https://geo.api.gouv.fr/regions";

// Defining async function
async function getapi(url) {
	
	// Storing response
	const response = await fetch(url);
	
	// Storing data in form of JSON
	var data = await response.json();
	console.log(data);
	if (response) {
		hideloader();
	}
	
	show(data);
}

// Calling that async function
getapi(api_url);

function hideloader() {
	document.getElementById('loading').style.display = 'none';
}

function show(data) {
	let tab =
		`<tr>
		<th>Name</th>
		<th>Code</th>
		</tr>`;
	
	// Loop to access all rows
	for (let r of data) {
		tab += `<tr>
	<td>${r.nom} </td>
	<td>${r.code}</td>		
	</tr>`;
	}

	// Setting innerHTML as tab variable
document.getElementById("regions").innerHTML = tab;
}